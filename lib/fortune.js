var fortuneCookies = [
    "你今天的運氣很好喔！",
    "你今天的運氣普普通通。",
    "我有預感你今天為中大獎",
    "如果你能請我喝飲料，那麼今天運氣就會很好！",
    "你今天運氣將不會太好。",
    "你今天運氣很差喔～",
];

exports.getFortune = function() {
    var idx = Math.floor(Math.random() * fortuneCookies.length);
    return fortuneCookies[idx];
};
